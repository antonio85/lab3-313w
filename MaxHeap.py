class MaxHeap(object):

    def __init__(self, size):
        # finish the constructor, given the following class data fields:
        self.__maxSize = size
        self.__length = 0
        self.__heap = []

    ''' free helper functions '''
    def getHeap(self):
        return self.__heap

    def getMaxSize(self):
        return self.__maxSize

    def setMaxSize(self, ms):
        self.__maxSize = ms
        # May need to add more code here if you want to use this method

    def getLength(self):
        return self.__length

    def setLength(self, l):
        self.__length = l

    ''' Required Methods '''
    def insert(self, data):
        # Insert an element into your heap.

        # When adding a node to your heap, remember that for every node n,
        # the value in n is greater than or equal to the values of its children,
        # but your heap must also maintain the correct shape.
        if self.__length < self.__maxSize:
            return

        self.__heap.append(data)
        self.__length += 1
        self.__heapify(1)

    def maximum(self):
        # return the max value in the heap
        print(self.__heap[0])
        return self._heap[0]

    def extractMax(self):
        # remove and return the maximum value in the heap
        temp = self.__heap[0]
        print(temp)
        self.__heapify(2)



    def __heapify(self, node):
        # helper function for reshaping the array
        if node == 1:#when a new element is added.
            lastAdded = self.__length - 1
            parentLastAdded = (lastAdded-1)/2
            while self.__heap[lastAdded]<self.__heap[parentLastAdded]:
                if self.__heap[lastAdded]<self.__heap[parentLastAdded]:
                    swap(lastAdded, parentLastAdded)
                    lastAdded = parentLastAdded
                else:
                    return
        elif node == 2:#when fuction extract maximum and it needs replaced by last item in the heap (the smallest one)
            if self.__length == 0:
                return
            lastAdded = self.getLength()-1
            self.__myHeap[0] = self.__myHeap[lastAdded]
            top = 0
            self.__length -= 1

            while true:
                leftChild = 2*top+1
                rightChild = 2*top+2

                if leftChild>self.__length-1 and rightChild>self.__length-1:
                    return
                if self.__heap[leftChild] > self.__heap[leftChild]:
                    higher = leftChild
                else:
                    higher = rightChild

                if top >-1 and self.__heap[higher]>self.__heap[top]:
                    swap(higher, top)
                    top = higher
                else:
                    return


    ''' Optional Private Methods can go after this line '''
    # If you see yourself using the same ~4 lines of code more than once...
    # You probably want to turn them into a method.

    def __swap(self, indexUno, indexDos):
        temp = self.__heap[indexOne]
        self.__heap[indexUno] = self.__heap[indexDos]
        self.__heap[indexDos] = temp

    def __printer(self):
        for x in self.__heap:
            print(x)
